#!/bin/bash

echo -e "\e[34m=====> Clean up from previous VM instance if any"
rm -rf $APACHE_LOGS_DIR
rm -rf $CLEAN_WWW

echo -e "\e[34m=====> Make Dirs"
mkdir -p $APACHE_LOGS_DIR
mkdir -p $CLEAN_WWW

echo -e "\e[34m=====> Updating apt-get"
sudo apt-get update > /dev/null
apt-get install -y apt-transport-https ca-certificates > /dev/null


echo -e "\e[34m=====> Installing GIT and tools"
sudo apt-get -y install vim git zip curl wget > /dev/null

echo -e "\e[34m=====> Setting default git user"
echo "[user]
	email = ${GIT_EMAIL}
	name = ${GIT_NAME}
[core]
	excludesfile = ${USER_HOME}.gitignore_global" | sudo tee $USER_HOME.gitconfig > /dev/null
chown vagrant:vagrant $USER_HOME.gitconfig


echo -e "\e[34m=====> writing a .gitignore_global"
echo '
### macOS ###
*.DS_Store
.AppleDouble
.LSOverride

# Thumbnails
._*

# Files that might appear in the root of a volume
.DocumentRevisions-V100
.fseventsd
.Spotlight-V100
.TemporaryItems
.Trashes
.VolumeIcon.icns
.com.apple.timemachine.donotpresent

# Directories potentially created on remote AFP share
.AppleDB
.AppleDesktop
Network Trash Folder
Temporary Items
.apdisk
'  | sudo tee $USER_HOME.gitignore_global > /dev/null
chown vagrant:vagrant $USER_HOME.gitignore_global





echo -e "\e[34m=====> Installing PHP & Apache"
sudo apt-get -y install php php-mcrypt php-imap php-curl php-cli php-mysql php-gd php-intl php-xml libapache2-mod-php7.0 php-dev php-pear apache2 > /dev/null

sudo a2enmod rewrite > /dev/null
sudo service apache2 restart > /dev/null

echo -e "\e[34m=====> apt-cache policy apache2"
apt-cache policy apache2

echo -e "\e[34m=====> php -v"
php -v


echo -e "\e[34m=====> Installing Mysql"
sudo apt-get install -y mariadb-server > /dev/null


# creates db in case needed outside of buildkit
echo -e "\e[34m=====> creating blank db"
mysql -uroot -proot -e "CREATE DATABASE ${DB_NAME};FLUSH PRIVILEGES;"
mysql -uroot -proot -e "USE mysql;UPDATE user SET plugin = 'mysql_native_password' WHERE user.Host = 'localhost' AND user.User = 'root';"
sudo mysqladmin --user=root password root

echo -e "\e[34m=====> Updating mysql conf"
echo '
[mysqld]
sql_mode=NO_ENGINE_SUBSTITUTION
' | sudo tee --append /etc/mysql/my.cnf > /dev/null
sudo service mysql restart











echo -e "\e[34m=====> Configure Apache"
echo '
ServerName localhost
' | sudo tee --append /etc/apache2/apache2.conf > /dev/null

echo "<VirtualHost *:80>
	ServerName ${VM_HOST_NAME}
	ServerAlias ${VM_HOST_NAME_SHORT}
	ServerAdmin webmaster@localhost
	DocumentRoot ${CLEAN_WWW}

	ErrorLog ${APACHE_LOGS_DIR}error.log
	CustomLog ${APACHE_LOGS_DIR}access.log combined

	<Directory ${CLEAN_WWW}>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
	</Directory>
</VirtualHost>" | sudo tee /etc/apache2/sites-available/$VM_HOST_NAME_SHORT.conf > /dev/null


# prevent redirection to ISP, this ip must match that specified in the VagrantFile
echo -e "\e[34m=====> modifying /etc/hosts"
echo "
${VM_PRIVATE_IP} ${VM_HOST_NAME}
${VM_PRIVATE_IP} ${VM_HOST_NAME_SHORT}
" | sudo tee /etc/hosts > /dev/null


echo -e "\e[34m=====> Restart Apache"
sudo a2ensite $VM_HOST_NAME_SHORT.conf > /dev/null
sudo service apache2 restart > /dev/null





# mysql-server must first be installed for the replace cmd
echo -e "\e[34m=====> Change apache user to vagrant"
sudo replace "export APACHE_RUN_USER=www-data" "export APACHE_RUN_USER=vagrant" -- /etc/apache2/envvars
sudo replace "export APACHE_RUN_GROUP=www-data" "export APACHE_RUN_GROUP=vagrant" -- /etc/apache2/envvars










echo -e "\e[34m=====> Configure SSH"
# Implement SSH keys for bitbucket
if [ ! -d /root/.ssh ]
	then mkdir -p /root/.ssh
fi
if [ ! -d $VAGRANT_HOME_SSH ]
	then mkdir -p $VAGRANT_HOME_SSH
fi
sudo cp $VMROOT$PRIVATE_KEY /root/.ssh/$PRIVATE_KEY
sudo cp $VMROOT$PRIVATE_KEY $VAGRANT_HOME_SSH/$PRIVATE_KEY

sudo chmod 0600 /root/.ssh/$PRIVATE_KEY
sudo chmod 0600 $VAGRANT_HOME_SSH/$PRIVATE_KEY

echo "Host bitbucket.org
 IdentitiesOnly yes
 StrictHostKeyChecking no
 IdentityFile /root/.ssh/${PRIVATE_KEY}" | sudo tee /root/.ssh/config > /dev/null

echo "Host bitbucket.org
 IdentitiesOnly yes
 StrictHostKeyChecking no
 IdentityFile ${VAGRANT_HOME_SSH}/${PRIVATE_KEY}" | sudo tee $VAGRANT_HOME_SSH/config > /dev/null

sudo chown vagrant:vagrant ${VAGRANT_HOME_SSH}/config
sudo chown vagrant:vagrant ${VAGRANT_HOME_SSH}/${PRIVATE_KEY}

curl -L https://github.com/vrana/adminer/releases/download/v4.6.3/adminer-4.6.3-mysql-en.php -o /vagrant/clean/adminer.php



echo -e "\e[34m=====> Install ADMINER.PHP"




sudo phpenmod mcrypt











echo -e "\e[34m=====> Misc Bash"
sudo replace "#force_color_prompt=yes" "force_color_prompt=yes" -- $USER_HOME.bashrc

echo "
alias ll='ls -lah'
alias ss='sudo service apache2 restart'
" | sudo tee --append $USER_HOME.bashrc > /dev/null



curl -LO https://getcomposer.org/composer.phar
sudo mv composer.phar /usr/local/bin/composer
sudo chmod +x /usr/local/bin/composer






sudo apt-get install -y php-xdebug

{ echo "zend_extension" | tr '\n' '=' ; find /usr/lib/ -name "xdebug.so"; } | sudo tee --append /etc/php/7.0/apache2/php.ini

echo "
xdebug.remote_host=${VM_HOST_NAME}
xdebug.remote_enable=1
xdebug.remote_connect_back=on
xdebug.idekey=PHPSTORM
" | sudo tee --append /etc/php/7.0/apache2/php.ini > /dev/null



echo -e "\e[34m=====> Restart Apache"
sudo service apache2 restart





echo -e "\e[44m   ,d8888b  d8,          "
echo -e "\e[44m   88P'    \`8P           "
echo -e "\e[44md888888P                 "
echo -e "\e[44m  ?88'      88b  88bd88b "
echo -e "\e[44m  88P       88P  88P' ?8b"
echo -e "\e[44m d88       d88  d88   88P"
echo -e "\e[44md88'      d88' d88'   88b"
