# Clean debian/stretch64 contrib with vbox support
Includes LAMP stack, phpmyadmin, configures GIT and SSH.

Apache2, MariaDB and PHP 7.0

### Purpose
Quick LAMP box for non-js development.

### How do I get set up?
* Place your private SSH key beside Vagrantfile and update the Vagrantfile with the name of your key.
* Other variables in Vagrantfile should be updated like git email and name.

### Who do I talk to?
* carlos@letuschasethesun.com

### Notes
* You must manually manage /etc/hosts on your host machine

