#! /bin/bash

cd $CLEAN_WWW
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
php wp-cli.phar --info
chmod +x wp-cli.phar
sudo mv wp-cli.phar /usr/local/bin/wp

cd $CLEAN_WWW
wp core download --allow-root


# create dev wpconfig
wp core config --dbname=$WP_DB_NAME --dbuser=$WP_DB_USER --dbpass=$WP_DB_PASS --dbprefix=$WP_DB_PREFIX --dbcharset="utf8mb4" --extra-php="define( 'WP_DEBUG' , TRUE );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', false );

define('WP_MEMORY_LIMIT', '512M');
define( 'WP_MAX_MEMORY_LIMIT', '512M' );
//ini_set('memory_limit','512M');

define( 'AUTOSAVE_INTERVAL', 300 );
define( 'WP_POST_REVISIONS', 5 );
define( 'EMPTY_TRASH_DAYS', 7 );
define( 'WP_CRON_LOCK_TIMEOUT', 120 );

define( 'WP_SITEURL', 'http://${VM_HOST_NAME}/' );
define( 'WP_HOME', 'http://${VM_HOST_NAME}/' );

define( 'IS_DONATE', FALSE );

define( 'WP_AUTO_UPDATE_CORE', FALSE );" --force --skip-check --allow-root




echo '
# BEGIN WordPress
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
</IfModule>

# END WordPress' | sudo tee $CLEAN_WWW.htaccess > /dev/null




wp rewrite flush --hard

echo -e "\e[34m===== copying nlf17 theme ============="
git -C $CLEAN_WWW/wp-content/themes/ clone git@bitbucket.org:nlf17510/nlf17.git > /dev/null

echo -e "\e[34m===== copying nlf17 plugins ============="
git -C $CLEAN_WWW/wp-content/plugins/ clone git@bitbucket.org:nlf17510/nlf_carousel.git > /dev/null
git -C $CLEAN_WWW/wp-content/plugins/ clone git@bitbucket.org:nlf17510/nlf_cpts.git > /dev/null




echo -e "\e[34m================================================================="
echo -e "\e[34mDatabase Time"

echo -e "\e[34m=====> creating blank db"
mysql -uroot -proot -e "CREATE DATABASE ${WP_DB_NAME}; FLUSH PRIVILEGES;"

echo -e "\e[34m=====> Importing ${WP_SQL_FILE}"
mysql -uroot -proot $WP_DB_NAME < $VMROOT$WP_SQL_FILE

echo -e "Dont forget to import wp-content/uploads/"